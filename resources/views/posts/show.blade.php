@extends('layouts.app')

@section('content')
    <h1>{{$post->title}}</h1>
    @if($post->cover_image)
        <img style="width:100%" src="/storage/cover_images/{{$post->cover_image}}">
        <br>
    @endif
    <p>{!! $post->body !!}</p>
    <p>Stars: {{$post->stars}}</p>
    <small>Post created at {{$post->created_at}}</small>  
    <hr>          
    <a href="/posts" class="btn btn-default">Go Back</a>
    @guest
    @else
        @if(Auth::user()->id == $post->user_id)
            <a href="/posts/{{$post->id}}/edit" class="btn btn-default">Edit</a>

            {!! Form::open(['action' => ['PostsController@destroy', $post->id], 'method' => 'POST']) !!}
                {{ Form::hidden('_method', 'DELETE') }}
                {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
            {!! Form::close() !!}
        @endif
    @endguest

@endsection
