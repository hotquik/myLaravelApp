@extends('layouts.app')

@section('content')
    <h1>@if ($note) Edit @else Create @endif Note</h1>
    {!! Form::open(['action' => ($note ? ['NotesController@update', $note->id] : 'NotesController@store'), 'method' => 'POST']) !!}
        <div class='form-group'>
            {!! Form::label('title', 'Title') !!}
            {!! Form::text('title', ($note ? $note->title : ''), ['class' => 'form-control', 'placeholder' => 'Title']) !!}
        </div>
        <div class='form-group'>
            {!! Form::label('body', 'Body') !!}
            {!! Form::textarea('body', ($note ? $note->body : ''), ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Body Text']) !!}
        </div>
        @if($note) 
            {{ Form::hidden('_method', 'PUT') }} 
        @endif
        {{ Form::submit('Submit', ['class' => 'btn btn-primary']) }}
    {!! Form::close() !!}
    <a href="/notes" class="btn btn-default">Go Back</a>
@endsection