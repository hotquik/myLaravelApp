<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/*
Controller for Index, About and Services pages
*/
class PagesController extends Controller
{
    /*
    Controller function for Index page
    */
    public function index () {
        // A String can be returned:
        // return 'Index';

        // Render a template with a named parameter:
        // $title = 'Welcome to Laravel';
        // return view('pages.index')->with('title', $title);

        // Render a template with a dictionary (use $title in the template):
        $data = array('title' => 'Welcome to My App!');
        return view('pages.index')->with($data);
    }

    /*
    Controller function for About page
    */
    public function about () {
        return view('pages.about');
    }

    /*
    Controller function for Services page
    */
    public function services () {
        $data = array(
            'title' => 'Welcome to My App!',
            'services' => ['Frontend', 'Backend', 'SEO']
        );
        return view('pages.services')->with($data);
    }
}
