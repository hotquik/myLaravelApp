<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 
    // Return a single string:
    // function () { return "Hello World"; }

    // Return a view to render:
    // function () { return view('welcome'); }

    // Bind to a Controller@function:
    'PagesController@index'
);
Route::get('/about', 'PagesController@about');
Route::get('/services', 'PagesController@services');

/*
Route::get('/user/{id}/{name}', function ($id, $name) {    
    return "this is user $id $name";
});
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Bind the resource to index, create, edit, store, update and delete functions in the controller:
Route::resource('posts', 'PostsController');
Route::resource('notes', 'NotesController');