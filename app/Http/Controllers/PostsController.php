<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Illuminate\Support\Facades\Storage;
// For sql queries:
// use DB;

/*
Controller for Posts pages. Posts will be visible for guests
*/
class PostsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // Blocks guests from entering some routes by specifying exceptions
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    /*
    * Checks if the post belongs to the current user.
    *
    * @return bool
    */
    private function checkUserId($post) {
        // Check for correct user
        return ( auth()->user()->id === $post->user_id );
    }

    /*
    * Returns an Unauthorized Page redirect
    *
    * @return \Illuminate\Http\Response
    */
    private function unauthorizedPageRedirect() {
        return redirect('/posts')->with('error', 'Unauthorized Page') ;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get all rows from table:
        // $posts = Post::all();

        // Get rows with a WHERE condition
        // $posts = Post::where('stars', 3)->get();

        // Get top 1 rows, ordered:
        // $posts = Post::orderBy('updated_at', 'desc')->take(1)->get();

        // Shows all posts. Get a paginated list. Use $posts->links() in the template:
        $posts = Post::orderBy('updated_at', 'desc')->paginate(2);
        // Render the template view
        return view('posts.index')->with('posts', $posts);

        // Execute a sql query
        // $posts = DB::select('SELECT * FROM posts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Find a row by its primary key
        $post = Post::find($id);
        // Render the template view
        return view('posts.show')->with('post', $post);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Render the template view
        return view('posts.edit')->with('post', null);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Find a row by its primary key
        $post = Post::find($id);
        // Check post's user
        if (!$this->checkUserId($post)) {
            return $this->unauthorizedPageRedirect();
        }
        // Render the template view
        return view('posts.edit')->with('post', $post);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->save($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->save($request, $id);
    }

    private function save(Request $request, $id = null) {
        // Validate received data from the http request
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required',
            'stars' => 'required',
            'cover_image' => 'image|nullable|max:1999' // image type, can be null, max size in kb
            ]
        );

        // Handle File Upload
        if ($request->hasFile('cover_image')) {
            // Get filename with the extension
            $filenameWithExt = $request->file('cover_image')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('cover_image')->getClientOriginalExtension();
            // Filename to store
            $filenameToStore = $filename.'_'.time().'.'.$extension;
            // Upload imate
            $path = $request->file('cover_image')->storeAs('public/cover_images', $filenameToStore);
        } else {
            $filenameToStore = '';
        }

        // Create a new post or find an existing one
        $post = ($id ? Post::find($id) : new Post());
        // Check post's user
        if ($id && !$this->checkUserId($post)) {
            return $this->unauthorizedPageRedirect();
        }
        $post->title = $request->input('title');
        $post->body = $request->input('body');
        $post->stars = $request->input('stars');
        $post->user_id = auth()->user()->id; // currently logged user
        // TO DO: Create a button for removing current image
        // TO DO: delete old image when replacing for a new one
        $post->cover_image = ( $post->cover_image && $filenameToStore ? $filenameToStore : ($filenameToStore ? $filenameToStore : $post->cover_image ) );
        $post->save();
        // Redirect the response
        return redirect("/posts/{$post->id}")->with('success', ($id ? 'Post updated' : 'Post created'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Find a row by its primary key
        $post = Post::find($id);
        // Check post's user
        if (!$this->checkUserId($post)) {
            return $this->unauthorizedPageRedirect();
        }
        // If an image exists, delete it
        if ($post->cover_image) {
            // Delete image
            Storage::delete('public/cover_images/' . $post->cover_image);
        }
        // Delete the row from the database
        $post->delete();
        // Redirect the response
        return redirect('/posts')->with('success', 'Post deleted');
    }
}
