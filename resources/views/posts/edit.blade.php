@extends('layouts.app')

@section('content')
<h1>@if ($post) Edit @else Create @endif Post</h1>
    {!! Form::open(['action' => ($post ? ['PostsController@update', $post->id] : 'PostsController@store'), 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
        <div class='form-group'>
            {!! Form::label('title', 'Title') !!}
            {!! Form::text('title', ($post ? $post->title : ''), ['class' => 'form-control', 'placeholder' => 'Title']) !!}
        </div>
        <div class='form-group'>
            {!! Form::label('body', 'Body') !!}
            {!! Form::textarea('body', ($post ? $post->body : ''), ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Body Text']) !!}
        </div>
        <div class='form-group'>
            {!! Form::label('stars', 'Stars') !!}
            {!! Form::number('stars', ($post ? $post->stars : 0), ['class' => 'form-control', 'placeholder' => 'Stars']) !!}
        </div>
        <div class='form-group'>
            {!! Form::file('cover_image') !!}
        </div>
        @if($post) 
            {{ Form::hidden('_method', 'PUT') }} 
        @endif
        {{ Form::submit('Submit', ['class' => 'btn btn-primary']) }}
    {!! Form::close() !!}
    <a href="/posts" class="btn btn-default">Go Back</a>
@endsection