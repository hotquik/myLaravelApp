<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Note;

/*
Controller for Notes pages. Notes will only be visible for authorized users
*/
class NotesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // Blocks guests from entering all routes
        $this->middleware('auth');
    }

    /*
    * Checks if the note belongs to the current user.
    *
    * @return bool
    */
    private function checkUserId($note) {
        // Check for correct user
        return ( auth()->user()->id === $note->user_id );
    }

    /*
    * Returns an Unauthorized Page redirect
    *
    * @return \Illuminate\Http\Response
    */
    private function unauthorizedPageRedirect() {
        return redirect('/notes')->with('error', 'Unauthorized Page') ;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Only show current user notes. Get a paginated list. Use $notes->links() in the template:
        $notes = Note::where('user_id', auth()->user()->id)->orderBy('updated_at', 'desc')->paginate(2);
        // Render the template view
        return view('notes.index')->with('notes', $notes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Render the template view
        // Pass note as null for the Edit template!
        return view('notes.edit')->with('note', null);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Find a row by its primary key
        $note = Note::find($id);
        // Check note's user
        if (!$this->checkUserId($note)) {
            return $this->unauthorizedPageRedirect();
        }
        // Render the template view
        return view('notes.edit')->with('note', $note);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->save($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->save($request, $id);
    }

    private function save(Request $request, $id = null) {
        // Validate received data from the http request
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required'
            ]
        );

        // Create a new note or find an existing one
        $note = ($id ? Note::find($id) : new Note());
        // Check note's user
        if ($id && !$this->checkUserId($note)) {
            return $this->unauthorizedPageRedirect();
        }
        $note->title = $request->input('title');
        $note->body = $request->input('body');
        $note->user_id = auth()->user()->id; // currently logged user
        $note->save();
        // Redirect the response
        return redirect("/notes/{$note->id}")->with('success', ($id ? 'Note updated' : 'Note created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Find a row by its primary key
        $note = Note::find($id);
        // Check note's user
        if (!$this->checkUserId($note)) {
            return $this->unauthorizedPageRedirect();
        }
        // Render the template view
        return view('notes.show')->with('note', $note);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Find a row by its primary key
        $note = Note::find($id);
        // Check note's user
        if (!$this->checkUserId($note)) {
            return $this->unauthorizedPageRedirect();
        }
        // Delete the row from the database
        $note->delete();
        // Redirect the response
        return redirect('/notes')->with('success', 'Note deleted');
    }
}
