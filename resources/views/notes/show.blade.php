@extends('layouts.app')

@section('content')
    <h1>{{$note->title}}</h1>
    <p>{!! $note->body !!}</p>
    <small>Note created at {{$note->created_at}}</small>  
    <hr>          
    <a href="/notes" class="btn btn-default">Go Back</a>
    <a href="/notes/{{$note->id}}/edit" class="btn btn-default">Edit</a>

    {!! Form::open(['action' => ['NotesController@destroy', $note->id], 'method' => 'POST']) !!}
        {{ Form::hidden('_method', 'DELETE') }}
        {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
    {!! Form::close() !!}

@endsection
