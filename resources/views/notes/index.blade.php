@extends('layouts.app')

@section('content')
    <h1>Notes</h1>
    <a href="/notes/create" class="btn btn-default">Create Note</a>
    @if(count($notes)>0)
        @foreach($notes as $note)
            <div class="well">
                <h3><a href="/notes/{{$note->id}}">{{$note->title}}</a></h3>
                <small>Note created at {{$note->created_at}}</small>
            </div>
        @endforeach
        {{$notes->links()}}
    @else
        <p>No posts found.</p>
    @endif
@endsection
