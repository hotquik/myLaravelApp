
## Composer & Artisan

Create Laravel Project

    composer create-project laravel/laravel myLaravelApp

Installing Bootstrap

    composer require laravel/ui
    php artisan ui bootstrap
    php artisan ui bootstrap --auth
    // php artisan ui:auth

Create a link for storage (private) to public

    php artisan storage:link
Install Laravel Collective

    composer require laravelcollective/html
Install CKEditor

    //composer require unisharp/laravel-ckeditor
    composer require ckeditor/ckeditor
    composer update
## Artisan commands
    php artisan list    
    php artisan help migrate    
    php artisan make:controller TodosController    
    php artisan make:controller PostsController --resource // create routes for resource   
    php artisan make:model Todo -m // create model and migration   
    php artisan make:migration add_todos_to_db-table =todos    
    php artisan migrate    
    php artisan migrate:rollback
    php arsitan tinker // database helper with eloquent    
    php artisan route:list    
    php artisan make:migration add_user_id_to_posts
  
## ELOQUENT

    Use App\Todo;    
    $todo = new Todo();    
    $todo->title = 'Some Todo';    
    $todo->save();

## BLADE

    @extends('foldername.filename')  // use another template as layout
    
    @section('sectionname') // pass sections to the layout template
    @endsection('sectionname')  
    
    @if(count($services) > 0)
    @else
    @endif  
    
    @foreach($services as $service)
    @else
    @endforeach  
    
    @include('foldername.filename')  // include another template in this place

## NPM

    npm install    
    npm run dev    
    npm run watch // auto build css and js  

## CKEDITOR

    npm install ckeditor4    
    npm update
